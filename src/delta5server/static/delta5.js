function map_range(in_min, in_max, out_min, out_max, value) {
  return (value - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
function normalize_rssi(value) {
    return map_range(0, 300, 0, 100, value);
}
